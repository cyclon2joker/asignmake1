//
//  ViewController.m
//  ASignMake1
//
//  Created by pies on 2018/02/05.
//  Copyright © 2018年 pies. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "ViewController.h"

@interface ViewController (){
    AVAudioSession          *_session;
    AVAudioEngine           *_engine;
    AVAudioUnitSampler      *_sampler;
//    AVAudioUnitDistortion   *_distortion;
//    AVAudioUnitReverb       *_reverb;
    AVAudioPlayerNode       *_player;
    
    AVAudioFormat           *_aFmt;
    
    // the sequencer
    AVAudioSequencer        *_sequencer;
    
    Float32 **sinWaveBuffer1;
    Float32 **sinWaveBuffer2;
    Float32 **sinWaveBuffer3;
    Float32 **mixSinWaveBuffer;

    
//    double                  _sequencerTrackLengthSeconds;
//
//    // buffer for the player
    AVAudioPCMBuffer        *_playerLoopBuffer;

    
}


@property (weak, nonatomic) IBOutlet UITextField *txtSampleRate;

@property (weak, nonatomic) IBOutlet UITextField *txtHZ;

@property (weak, nonatomic) IBOutlet UITextField *txtHZ2;

@property (weak, nonatomic) IBOutlet UITextField *txtSec;
@property (weak, nonatomic) IBOutlet UISegmentedControl *modeMix;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupAudio];






}

- (void)setupAudio{
    
    _session = [AVAudioSession sharedInstance];
    NSError *error;
    
    // set the session category
    bool success = [_session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&error];
    if (!success) NSLog(@"Error setting AVAudioSession category! %@\n", [error localizedDescription]);
    
    success = [_session setPreferredSampleRate:44100. error:&error];
    if (!success) NSLog(@"Error setting preferred sample rate! %@\n", [error localizedDescription]);
    
    NSTimeInterval ioBufferDuration = 0.0029;
    success = [_session setPreferredIOBufferDuration:ioBufferDuration error:&error];
    if (!success) NSLog(@"Error setting preferred io buffer duration! %@\n", [error localizedDescription]);
    
    [_session setMode:AVAudioSessionModeMeasurement error:&error];
    
    // 割り込み系のコールバックはとりあえず実装しない
    success = [_session setActive:YES error:&error];
    if (!success) NSLog(@"Error setting session active! %@\n", [error localizedDescription]);
    
    
    
    _engine = nil;
    
    _player = [[AVAudioPlayerNode alloc] init];
    
    _sampler = [[AVAudioUnitSampler alloc] init];
    
    
    _engine = [[AVAudioEngine alloc] init];
    
    
//    AVAudioMixerNode *mixer = [_engine mainMixerNode];
//    AVAudioInputNode *input = [_engine inputNode];
    
    
    //    [_engine connect:input to:mixer format:[input inputFormatForBus:0]];
    
    //input ou
    
    
    
    /*  To support the instantiation of arbitrary AVAudioNode subclasses, instances are created
     externally to the engine, but are not usable until they are attached to the engine via
     the attachNode method. */
    
    //    [_engine attachNode:_sampler];
    //    [_engine attachNode:_distortion];
    //    [_engine attachNode:_reverb];

    [_engine attachNode:_player];
    _aFmt = [_player outputFormatForBus:0];

    AVAudioMixerNode *mixer = [_engine mainMixerNode];
    [_engine connect:_player to:mixer format:_aFmt];

    
    [_engine startAndReturnError:nil];

    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tapGen:(id)sender {
    
    __block Float32 sampleRate = [_txtSampleRate.text floatValue];
    __block Float32 sec = [_txtSec.text floatValue];
    __block Float32 p1 = [_txtHZ.text floatValue];
    __block Float32 p2 = [_txtHZ2.text floatValue];

    // サイン波用バッファ確保
    // とりあえず、モックなのでデタラメな秒数にしない事を前提。。。
    // とりあえず、和音は後回し
    NSUInteger sz = sec * sampleRate;
//    sinWaveBuffer1[0] = (Float32*)calloc(sz, sizeof(Float32));
//    sinWaveBuffer1[1] = (Float32*)calloc(sz, sizeof(Float32));

    
    // サイン波の値をLRざっくり作成
    
    
    __block AVAudioPCMBuffer *pcmBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:_aFmt frameCapacity:sz];
    [pcmBuffer setFrameLength:sz];
    
//    pcmBuffer.data
    Float32 *pt1 = pcmBuffer.floatChannelData[0];
    Float32 *pt2 = pcmBuffer.floatChannelData[1];
    __block BOOL isMixed = _modeMix.selectedSegmentIndex != 0;
//    Float32 amplitude = isMixed ? 1. / 2. : 1.;
    Float32 amplitude = isMixed ? 0.4 : 1.;

    for (NSUInteger idx = 0; idx < sz; idx++) {
        Float32 v = [self signWaveValue:p1 sampleRate:sampleRate frIdx:idx amplitude:amplitude];
        if (!isMixed) {
            pt1[idx] = v;
            pt2[idx] = v;
        } else {
            Float32 v2 = [self signWaveValue:p2 sampleRate:sampleRate frIdx:idx amplitude:amplitude];
            pt1[idx] = (v + v2)  ;
            pt2[idx] = (v + v2) ;
            if ((v + v2) > 1.) NSLog(@"detect over 1 idx:%u,%lf",idx,(v + v2));
        }
    }
//    sinf(Float(2.0 * M_PI) * 440.0 * Float(n) / sampleRate)
//    for n in 0..<Int(buffer.frameLength) {
//        samples[n] = sinf(Float(2.0 * M_PI) * 440.0 * Float(n) / sampleRate)
//    }

    __block __weak typeof(self) weakSelf = self;
    
    [_player scheduleBuffer:pcmBuffer completionHandler:^{

        NSString *path;
        if (!isMixed) {
            path = [NSString stringWithFormat:@"%@/Documents/sample_%d_%d.wav", NSHomeDirectory(), (int)p1, (int)sec];
        } else {
            path = [NSString stringWithFormat:@"%@/Documents/sample_%d_%d_%d.wav",
                    NSHomeDirectory(), (int)p1,(int)p2, (int)sec];
        }
        NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
        NSFileManager *fm = [NSFileManager defaultManager];
        if ([fm fileExistsAtPath:path]) {
            [fm removeItemAtPath:path error:nil];
        }
        AVAudioFile *fp = [[AVAudioFile alloc] initForWriting:url settings:_aFmt.settings error:nil];
        [fp writeFromBuffer:pcmBuffer error:nil];
        NSLog(@"write wav : %@",path);
        
    }];
    // 再生の開始を設定
    [_player play];
    
}

-(Float32)signWaveValue:(Float32)pitch
             sampleRate:(Float32)sampleRate
                frIdx:(NSUInteger)frIdx
              amplitude:(Float32)amplitude {
    return sinf(2. * M_PI * pitch * frIdx / sampleRate) * amplitude;
}




@end
